import fs from 'fs';

export default function toPinyin(data) {
    let content = data
        .split("\n")
        .map((line) => {
            let cityName = line.split(" - ")[0];
            let cityPinyin = line.split(" - ")[1]
                .replaceAll("http://lishi.tianqi.com/", "")
                .replaceAll("/index.html", "");
            return {
                "name": cityName,
                "py": cityPinyin
            }
        })

    fs.writeFileSync("cityPinyin.txt", JSON.stringify(content));
}

let s = fs.readFileSync('cities.txt').toString();
toPinyin(s)

