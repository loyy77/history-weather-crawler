import fs from 'fs';

function genMonthDays() {
    let year = ["2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022"];
    let month = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];

    let ret = [];
    for (let i = 0; i < year.length; i++) {
        for (let j = 0; j < month.length; j++) {
            ret.push(`${year[i]}${month[j]}`);
        }
    }
    return ret;
}


let cities = fs.readFileSync("cities.txt").toString().split("\n");
let cityMonthUrls = [];
let monthDays = genMonthDays();
cities.forEach(urlWithCityName => {
    monthDays.forEach((ym) => {
        let monthUrl = urlWithCityName.split(" - ")[1].replace("index", ym);
        cityMonthUrls.push(monthUrl);

    })
})
fs.writeFileSync("citiMonthUrl.txt", cityMonthUrls.join("\n"));
console.log('exit');
